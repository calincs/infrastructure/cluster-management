# Cluster Management

The GitLab Cluster Management project for the LINCS prod (production), stage (staging), and sandbox (development) clusters.

Reference: [https://docs.gitlab.com/ee/user/clusters/management_project.html](https://docs.gitlab.com/ee/user/clusters/management_project.html)

This project is based on [this](https://gitlab.com/gitlab-org/cluster-integration/example-cluster-applications/-/tree/master/.gitlab/managed-apps) GitLab [Project Template](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates).
